package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"go-lang-blog/model"

	"go-lang-blog/controller"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
)

var (
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	key   = []byte("super-secret-key")
	store = sessions.NewCookieStore(key)
)

func basicAuth(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var res model.ResponseResult

		session, _ := store.Get(r, "session-name")

		// Check if user is authenticated
		if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
			w.WriteHeader(http.StatusUnauthorized)
			res.Error = "Unauthorized"
			json.NewEncoder(w).Encode(res)
			return
		}
		// Print secret message
		fmt.Println("Session is okay", session.Values)
		h.ServeHTTP(w, r)
	}
}

func main() {

	router := mux.NewRouter()

	router.HandleFunc("/register", controller.RegisterHandler).
		Methods("POST")
	router.HandleFunc("/login", controller.LoginHandler).
		Methods("POST")
	router.HandleFunc("/logout", basicAuth(controller.LogoutHandler)).
		Methods("POST")
	router.HandleFunc("/profile", basicAuth(controller.ProfileHandler)).
		Methods("GET")
	router.HandleFunc("/blog", basicAuth(controller.UploadBlogHandler)).
		Methods("POST")
	router.HandleFunc("/blog", basicAuth(controller.GetBlogHandler)).
		Methods("GET")
	router.HandleFunc("/blog/remove", basicAuth(controller.RemoveBlogHandler)).
		Methods("GET")
	router.HandleFunc("/blog/update", basicAuth(controller.UpdateBlogHandler)).
		Methods("POST")

	log.Fatal(http.ListenAndServe(":8080", router))
}
