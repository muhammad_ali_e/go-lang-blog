package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"go-lang-blog/model"

	db "go-lang-blog/config"

	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	key   = []byte("super-secret-key")
	store = sessions.NewCookieStore(key)
)

//RegisterHandler :create a user in the Database
func RegisterHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	//Database connection  --start
	mongoSession := db.Mainconnection()
	defer mongoSession.Close()
	collection := mongoSession.DB("GoLogin").C("users")
	//Database connection  --end

	var res model.ResponseResult
	var user model.User
	var result model.User

	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &user)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error while parsing body: ", err.Error())
		res.Error = "Invalid params"
		json.NewEncoder(w).Encode(res)
		return
	}
	if len(user.Username) < 3 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error password: ", user.Username)
		res.Error = "Username - Should be atleast 3 characters"
		json.NewEncoder(w).Encode(res)
		return
	} else if len(user.Password) < 4 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error password: ", user.Password)
		res.Error = "Password - Should be atleast 4 characters"
		json.NewEncoder(w).Encode(res)
		return
	}

	err = collection.Find(bson.M{"username": user.Username}).One(&result)

	if err != nil {
		if !mgo.IsDup(err) {
			hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 5)

			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				res.Error = "Error While Hashing Password, Try Again"
				json.NewEncoder(w).Encode(res)
				return
			}
			user.Password = string(hash)
			err = collection.Insert(user)
			if err != nil {
				fmt.Println(err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				res.Error = "Error While Creating blog, Try Again"
				json.NewEncoder(w).Encode(res)
				return
			}
			w.WriteHeader(http.StatusOK)
			res.Result = "Create user Successful"
			json.NewEncoder(w).Encode(res)
			return
		}
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		res.Error = "Internal Error"
		json.NewEncoder(w).Encode(res)
		return
	}
	w.WriteHeader(http.StatusConflict)
	res.Result = "username already Exists!!"
	json.NewEncoder(w).Encode(res)
	return

}

//LoginHandler : Fuction to Login to the user and create a session
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//Database connection  --start
	mongoSession := db.Mainconnection()
	defer mongoSession.Close()
	collection := mongoSession.DB("GoLogin").C("users")
	//Database connection  --end

	var res model.ResponseResult
	var user model.User
	var result model.User

	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &user)

	if err != nil {
		fmt.Println("Error while parsing body: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		res.Error = "Invalid params"
		json.NewEncoder(w).Encode(res)
		return
	}

	if len(user.Username) < 3 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error password: ", user.Username)
		res.Error = "Invalid username"
		json.NewEncoder(w).Encode(res)
		return
	} else if len(user.Password) < 4 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error password: ", user.Password)
		res.Error = "Invalid password"
		json.NewEncoder(w).Encode(res)
		return
	}

	err = collection.Find(bson.M{"username": user.Username}).One(&result)

	if err != nil {
		fmt.Println("Error login user: ", err.Error())
		w.WriteHeader(http.StatusUnauthorized)
		res.Error = "Invalid username"
		json.NewEncoder(w).Encode(res)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(user.Password))

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		res.Error = "Invalid password"
		json.NewEncoder(w).Encode(res)
		return
	}

	sess, _ := store.Get(r, "session-name")
	sess.Values["authenticated"] = true
	sess.Values["username"] = result.Username
	sess.Save(r, w)

	//result.Token = tokenString
	w.WriteHeader(http.StatusOK)
	result.Password = "-"
	json.NewEncoder(w).Encode(result)
	return
}

//ProfileHandler : Get the profile of current user
func ProfileHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	//Database connection  --start
	mongoSession := db.Mainconnection()
	defer mongoSession.Close()
	collection := mongoSession.DB("GoLogin").C("users")
	//Database connection  --end

	var res model.ResponseResult
	var profile model.Profile

	//session handler  --start
	sess, _ := store.Get(r, "session-name")
	username := sess.Values["username"]
	//session handler  --end

	err := collection.Find(bson.M{"username": username}).One(&profile)
	if err != nil {
		res.Error = "Invalid user"
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(res)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(profile)
	return

}

//UploadBlogHandler : push the blog into databse
func UploadBlogHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	//Database connection  --start
	mongoSession := db.Mainconnection()
	defer mongoSession.Close()
	collection := mongoSession.DB("GoLogin").C("blogs")
	//Database connection  --start

	var blog model.Blog
	var res model.ResponseResult

	//session handler  --start
	sess, _ := store.Get(r, "session-name")
	username, _ := sess.Values["username"].(string)
	//session handler  --end

	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &blog)

	if err != nil {
		res.Error = "Invalid Params"
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(res)
		return
	}
	blog.Username = username

	// VALIDATION --- Start
	if len(blog.Title) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error title: ", blog.Title)
		res.Error = "Invalid title - Empty"
		json.NewEncoder(w).Encode(res)
		return
	} else if len(blog.Content) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error Content: ", blog.Content)
		res.Error = "Invalid Content - Empty"
		json.NewEncoder(w).Encode(res)
		return
	} else if len(blog.Author) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error Author: ", blog.Author)
		res.Error = "Invalid Author - Empty"
		json.NewEncoder(w).Encode(res)
		return
	}
	// VALIDATION --- End

	err = collection.Find(bson.M{"username": blog.Username, "title": blog.Title}).One(&blog)

	if err != nil {
		if !mgo.IsDup(err) {
			blog.CreatedAt = time.Now()
			blog.UpdatedAt = time.Now()
			err = collection.Insert(blog)
			if err != nil {
				fmt.Println(err.Error())
				fmt.Println("00000000")
				w.WriteHeader(http.StatusInternalServerError)
				res.Error = "Error While Creating blog, Try Again"
				json.NewEncoder(w).Encode(res)
				return
			}
			w.WriteHeader(http.StatusOK)
			res.Result = "updation Successful"
			json.NewEncoder(w).Encode(res)
			return
		}
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		res.Error = "Internal error"
		json.NewEncoder(w).Encode(res)
		return
	}
	w.WriteHeader(http.StatusConflict)
	res.Result = "title already Exists!!"
	json.NewEncoder(w).Encode(res)
	return
}

//UpdateBlogHandler : update the blog into databse
func UpdateBlogHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	//Database connection  --start
	mongoSession := db.Mainconnection()
	defer mongoSession.Close()
	collection := mongoSession.DB("GoLogin").C("blogs")
	//Database connection  --start

	var blog model.Blog
	var res model.ResponseResult

	//session handler  --start
	sess, _ := store.Get(r, "session-name")
	username, _ := sess.Values["username"].(string)
	//session handler  --end

	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &blog)

	if err != nil {
		res.Error = "Invalid Params"
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(res)
		return
	}
	blog.Username = username
	blog.UpdatedAt = time.Now()

	// VALIDATION --- Start
	if len(blog.ID) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error ID: ", blog.ID)
		res.Error = "Invalid ID - Empty"
		json.NewEncoder(w).Encode(res)
		return
	} else if len(blog.Content) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error Content: ", blog.Content)
		res.Error = "Invalid Content - Empty"
		json.NewEncoder(w).Encode(res)
		return
	}
	// VALIDATION --- End

	err = collection.Update(bson.M{"_id": blog.ID, "username": blog.Username},
		bson.M{"$set": bson.M{"content": blog.Content, "updated_at": blog.UpdatedAt}})

	if err != nil {
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		res.Error = "Not updated"
		json.NewEncoder(w).Encode(res)
		return
	}
	w.WriteHeader(http.StatusOK)
	res.Result = "Success fully updated"
	json.NewEncoder(w).Encode(res)
	return
}

//GetBlogHandler : Get the blog from database
func GetBlogHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	//Database connection  --start
	mongoSession := db.Mainconnection()
	defer mongoSession.Close()
	collection := mongoSession.DB("GoLogin").C("blogs")
	//Database connection  --end

	var results []*model.Blog
	var res model.ResponseResult
	var blog model.Blog
	var result model.Blog

	//session handler  --start
	sess, _ := store.Get(r, "session-name")
	username, _ := sess.Values["username"].(string)
	//session handler  --end

	var page int
	var Limit int
	blog.Username = username

	pg, ok := r.URL.Query()["page"]
	if !ok || len(pg[0]) < 1 {
		fmt.Println("Url Param 'page' is missing")
	} else {
		page, _ = strconv.Atoi(pg[0])
	}
	lt, ok := r.URL.Query()["limit"]
	if !ok || len(lt[0]) < 1 {
		fmt.Println("Url Param 'limit' is missing")
	} else {
		Limit, _ = strconv.Atoi(lt[0])
	}
	uname, ok := r.URL.Query()["username"]
	if !ok || len(uname[0]) < 1 {
		fmt.Println("Url Param 'uname' is missing")
	} else {
		username = uname[0]
	}

	blog.Username = username

	fmt.Println("==page= Limit=>", page, Limit)

	if page != 0 && Limit != 0 {
		skip := (page - 1) * Limit
		blogs := make([]*model.Blog, Limit)

		err := collection.Find(bson.M{"username": blog.Username}).Sort("-updated_at").Skip(skip).Limit(Limit).All(&blogs)
		if err != nil {
			fmt.Println("nothing found 2")
			res.Error = "Not Found"
			w.WriteHeader(http.StatusNotFound)
			json.NewEncoder(w).Encode(res)
			return
		}

		fmt.Printf("Found multiple documents (array of pointers): %+v\n", results)
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(blogs)
	} else {
		var title string
		var blogid string
		ttl, ok := r.URL.Query()["title"]
		if !ok || len(ttl[0]) < 1 {
			fmt.Println("Url Param 'title' is missing")
		} else {
			title = ttl[0]
		}
		bgid, ok := r.URL.Query()["id"]
		if !ok || len(bgid[0]) < 1 {
			fmt.Println("Url Param 'id' is missing")
		} else {
			blogid = bgid[0]
		}
		if len(title) > 0 {

			blog.Title = title
			fmt.Println("blog", blog)
			err := collection.Find(bson.M{"username": blog.Username, "title": blog.Title}).One(&result)
			if err != nil {
				w.WriteHeader(http.StatusNotFound)
				res.Error = "Blog not found"
				json.NewEncoder(w).Encode(res)
				return
			}
		} else if len(blogid) > 0 {
			blog.ID = bson.ObjectIdHex(blogid)
			fmt.Println("blog", blog)
			err := collection.Find(bson.M{"username": blog.Username, "_id": blog.ID}).One(&result)
			if err != nil {
				w.WriteHeader(http.StatusNotFound)
				res.Error = "Blog not found"
				json.NewEncoder(w).Encode(res)
				return
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
			res.Error = "Missing params"
			json.NewEncoder(w).Encode(res)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(result)
	}
	return

}

//RemoveBlogHandler : Remove or delete Blog from databse
func RemoveBlogHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	//Database connection  --start
	mongoSession := db.Mainconnection()
	defer mongoSession.Close()
	collection := mongoSession.DB("GoLogin").C("blogs")
	//Database connection  --end

	var res model.ResponseResult
	var blog model.Blog

	//session handler  --start
	sess, _ := store.Get(r, "session-name")
	username, _ := sess.Values["username"].(string)
	//session handler  --end

	blog.Username = username
	var blogid string
	var flush bool
	fls, ok := r.URL.Query()["flush"]
	if !ok || len(fls[0]) < 1 {
		fmt.Println("Url Param 'flush' is missing")
	} else {
		flush, _ = strconv.ParseBool(fls[0])
	}

	bgid, ok := r.URL.Query()["id"]
	if !ok || len(bgid[0]) < 1 {
		fmt.Println("Url Param 'blogid' is missing")
	} else {
		blogid = bgid[0]
	}
	fmt.Println("==flush= blogid=>", flush, blogid)
	blog.Username = username

	if flush == true {

		fmt.Println("blog", blog)
		result, err := collection.RemoveAll(bson.M{"username": blog.Username})
		fmt.Println("Result  ", result)
		if err != nil {
			res.Error = "Internal Error"
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Println(err.Error())
		} else {
			w.WriteHeader(http.StatusOK)
			res.Result = "Successfully flushed your data"
		}
		json.NewEncoder(w).Encode(res)
		return

	} else if len(blogid) > 0 {
		blog.ID = bson.ObjectIdHex(blogid)
		err := collection.Remove(bson.M{"username": blog.Username, "_id": blog.ID})

		if err != nil {
			fmt.Println(err.Error())
			if err.Error() == "not found" {
				w.WriteHeader(http.StatusInternalServerError)
				res.Error = "ID Not found"
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				res.Error = "Internal Error"
			}

		} else {
			w.WriteHeader(http.StatusOK)
			res.Result = "Successfully removed : " + blogid
		}
		json.NewEncoder(w).Encode(res)
		return

	} else {
		w.WriteHeader(http.StatusBadRequest)
		res.Error = "Missing params"
		json.NewEncoder(w).Encode(res)
		return
	}

}

//LogoutHandler : signing out from the session
func LogoutHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	var res model.ResponseResult

	//session handler  --start
	session, _ := store.Get(r, "session-name")
	session.Values["authenticated"] = false
	session.Save(r, w)
	//session handler  --end

	w.WriteHeader(http.StatusOK)
	res.Result = "Loged-out"
	json.NewEncoder(w).Encode(res)
	return

}
