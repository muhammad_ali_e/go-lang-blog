module go-lang-blog

go 1.12

require (
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/sessions v1.2.0
	golang.org/x/crypto v0.0.0-20200320181102-891825fb96df
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
