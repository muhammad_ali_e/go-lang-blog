package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//User : user model
type User struct {
	Username  string `json:"username"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
	Password  string `json:"password"`
}

//Profile : Profile model
type Profile struct {
	Username  string `json:"username"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
}

//Blog : Blog model
type Blog struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	Username  string        `json:"username"`
	Title     string        `json:"title"`
	Content   string        `json:"content"`
	Author    string        `json:"author"`
	CreatedAt time.Time     `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time     `json:"updated_at" bson:"updated_at"`
}

//ResponseResult : response model
type ResponseResult struct {
	Error  string `json:"error"`
	Result string `json:"result"`
}
