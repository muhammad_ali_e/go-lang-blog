package db

import (
	"gopkg.in/mgo.v2"
)

// Mainconnection create a mongo session
func Mainconnection() *mgo.Session {
	session, err := mgo.Dial("mongodb://localhost:27017")
	check(err)
	//defer session.Close()
	return session
}
func check(err error) {
	if err != nil {
		panic(err)
	}
}
