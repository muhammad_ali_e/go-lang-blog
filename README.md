# go-lang-blog

application for posting and reading the blogs

* Developed with go-lang
* For Documentation :  Api-doc.pdf

## Build

*`go build`*

## Run

*`./go-lang-blog`*
