# API Docs

>## General

|            |                  |
| ---------- | ---------------- |
| URl        | `127.0.0.1:8080` |

>## register user 

*`register a user`*

|        |             |
| ------ | ----------- |
| API    | `/register` |
| Method | `POST`      |

>  Body Params

| Params    | Type     |            |
| --------- | -------- | ---------- |
| username  | `String` |            |
| password  | `String` |            |
| firstname | `String` | `optional` |
| lastname  | `String` | `optional` |

>  Response

| Status-code | status        |
| ----------- | ------------- |
| 200         | `Success`     |
| 400         | `Bad request` |
| 409         | `conflict`    |

>## login user 

*`login a user`*

|        |          |
| ------ | -------- |
| API    | `/login` |
| Method | `POST`   |

>  Body Params

| Params   | Type     |     |
| -------- | -------- | --- |
| username | `String` |     |
| password | `String` |     |

>  Response

| Status-code | status         |
| ----------- | -------------- |
| 200         | `Success`      |
| 400         | `Bad request`  |
| 401         | `Unauthorized` |

>## please login for following APIs

>## Logout

*`logout from a user session`*
|        |           |
| ------ | --------- |
| API    | `/logout` |
| Method | `POST`    |

>  Response

| Status-code | status         |
| ----------- | -------------- |
| 200         | `Success`      |
| 400         | `Bad request`  |
| 401         | `Unauthorized` |

>## User Profile

*`Get current user profile`*

|        |            |
| ------ | ---------- |
| API    | `/profile` |
| Method | `GET`      |


>  Response

| Status-code | status         |
| ----------- | -------------- |
| 200         | `Success`      |
| 400         | `Bad request`  |
| 401         | `Unauthorized` |

>## Create Blog

*`Post a blog into the Server`*

|        |         |
| ------ | ------- |
| API    | `/blog` |
| Method | `POST`  |

>  Body Params

| Params  | Type     |
| ------- | -------- |
| title   | `String` |
| content | `String` |
| author  | `String` |

>  Response

| Status-code | status         |
| ----------- | -------------- |
| 200         | `Success`      |
| 400         | `Bad request`  |
| 401         | `Unauthorized` |

>## Get Blog

*`Get a blog/ blogs from Server`*

|        |         |
| ------ | ------- |
| API    | `/blog` |
| Method | `GET`   |

>  QUERY Params

| Params   | Type     |                                                                           |
| -------- | -------- | ------------------------------------------------------------------------- |
| page     | `number` |                                                                           |
| limit    | `number` | `number of blogs in a page`                                               |
| title    | `String` | `Get Blog of current user with the title name --(No page & limit) Single` |
| username | `String` | `Get Blogs of another user  -- Need page & limit`                         |
| id       | `String` | `ID of the blog -- May Join with username param`                          |


>  Response

| Status-code | status         |
| ----------- | -------------- |
| 200         | `Success`      |
| 400         | `Bad request`  |
| 401         | `Unauthorized` |
| 404         | `Not Found`    |

>## Update Blog

*`Update a blog content`*

|        |                |
| ------ | -------------- |
| API    | `/blog/update` |
| Method | `POST`         |

>  Body Params

| Params | Type     |
| ------ | -------- |
| title  | `String` |
| id     | `String` |

>  Response

| Status-code | status         |
| ----------- | -------------- |
| 200         | `Success`      |
| 400         | `Bad request`  |
| 401         | `Unauthorized` |

>## Remove Blog

*`Remove a blog/ Flush all blogs  from serve`*

|        |                |
| ------ | -------------- |
| API    | `/blog/remove` |
| Method | `GET`          |

>  Body Params

| Params | Type     |                                            |
| ------ | -------- | ------------------------------------------ |
| flush  | `String` | `optional : used to flush the DB`          |
| id     | `String` | `optional used to remove the blog with ID` |

>  Response

| Status-code | status         |
| ----------- | -------------- |
| 200         | `Success`      |
| 400         | `Bad request`  |
| 401         | `Unauthorized` |